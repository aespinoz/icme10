Software set-up
=================


**Clone software repository**

Clone the scripts on your computer::
	
	git clone https://gitlab.com/aespinoz/icme10.git

**Install software and dependencies**::

	cd icme10
	./built_deps.sh

**Install MEGAN tools**::

	chmod a+x ./deps/megan/MEGAN_Community_unix_6_12_3.sh
	./deps/megan/MEGAN_Community_unix_6_12_3.sh

and follow teacher instructions.

**Setting variables**

In the file "conf_cluster.conf" you can set environment variables and bioinformatic parameters for the analysis. 

Open the file with an editor::

	vim conf_cluster.conf 

and modify it following the instructions of the instructor.

To make the changes active source the file::

	source conf_cluster.conf 
