International Course in Microbial Ecology ICME10
==========================================

Welcome to the practical course “Microbial ecology: Hands-on training in prokaryotic and eukaryotic metagenomics (ICME-10)”, Milan 6-10 May 2019.

This Practical Course is aimed at PhD students, post-docs, and researchers with some experience in the field of metagenomics and who are seeking to improve their skills. Beginners are also welcome and will be introduced to Linux command Shell and main command line. For further information about the corse, visit https://www.microbeco.org/portfolio-items/10-icme/This practical aims at running a complete bioinformatic analysis of a metagenomics dataset.

The practical is based of the software repository https://gitlab.com/aespinoz/icme10 which contains all the third-part software and dependencies to run a complete bioinformatic analysis of a metagenomics dataset.

The following programs are used for the analysis:

* Quality Filtering: Sickle (https://github.com/najoshi/sickle)
* Assembly: IDBA-UD (https://github.com/loneknightpy/idba)
* Gene prediction: Prodigal (https://github.com/hyattpd/Prodigal)
* Annotation: DIAMOND (https://github.com/bbuchfink/diamond)
* Alignment: Bowtie2 (http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
* Alignment file manipulation: Samtools, Bedtools (http://samtools.sourceforge.net/; http://bedtools.readthedocs.io/en/latest/)
* Taxonomic assignemts and parsing: MEGAN, Krona, in-house scritps (http://ab.inf.uni-tuebingen.de/software/megan6/; https://github.com/marbl/Krona/wiki)
* Graphical output: Krona


The present documentation helps the students to run the whole pipiline on the computer cluster.

Contents:

.. toctree::
   :maxdepth: 2

   conda
   ip
   bash_tutorial_sandionigi
   software   
   trimming
   assembly
   prodigal
   annotation
   mapping 
   parsing
   mining	
